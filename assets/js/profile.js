let token = localStorage.getItem("token");

let profileContainer = document.querySelector('#profileContainer')


if(!token || token === null){

	alert('You must login first');
	window.location.replace("./login.html");

} else {

	fetch('https://peaceful-tundra-33461.herokuapp.com/api/users/details', {
		headers: {
			'Authorization': `Bearer ${token}`
		}
	})
	.then(res => res.json())
	.then(data => {
		console.log(data);

		profileContainer.innerHTML = 
		`
			<div class="col-md-12">
				<section class="jumbotron my-5">		
					<h3 class="text-center">First Name: ${data.firstName}</h3>
					<h3 class="text-center">Last Name: ${data.lastName}</h3>
					<h3 class="text-center">Email: ${data.email}</h3>
					<h3 class="text-center">Email: ${data.mobileNo}</h3>
					<h3 class="mt-5">Class History</h3>

					<table class="table">
						<thead>
							<tr>
								<th> Course ID </th>
								<th> Enrolled On </th>
								<th> Status </th>
							</tr>
						</thead>

						<tbody id="coursesContainer">

						</tbody>
					</table> 

				</section>
			</div>
		`

		let coursesContainer = document.querySelector('#coursesContainer');
		console.log(coursesContainer);

		data.enrollments.map(enrollment => {

			console.log(enrollment);

			fetch(`https://peaceful-tundra-33461.herokuapp.com/api/courses/${enrollment.courseId}`)
			.then(res => res.json())
			.then(data => {
				console.log(data);

				coursesContainer.innerHTML += // '+=' since we use map, the function is applied/looped to all iterations. If just '=' it will overlap the first iteration, resulting in 1 output

				`
					<tr>
						<td>${data.name}</td>
						<td>${enrollment.enrolledOn}</td>
						<td>${enrollment.status}</td>
					</tr>
				`
			})

		})
	})

}
