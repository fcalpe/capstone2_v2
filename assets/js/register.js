let registerForm = document.querySelector('#registerUser');

registerForm.addEventListener("submit", (e) => {
	//prevents the form to revert to it's default/blank value
	e.preventDefault();

	let firstName = document.querySelector("#firstName").value;
	let lastName = document.querySelector("#lastName").value;
	let mobileNumber = document.querySelector("#mobileNumber").value;
	let userEmail = document.querySelector("#userEmail").value;
	let password1 = document.querySelector("#password1").value;
	let password2 = document.querySelector("#password2").value;

	if((password1 !== '' && password2 !== '') && (password1 === password2) && (mobileNumber.length === 11)){

		//fetch('url', {options})
		//used to process a request
		fetch('https://peaceful-tundra-33461.herokuapp.com/api/users/email-exists', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: userEmail
				//email="ferosa@gmail.com"
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if (data === false){

				fetch('https://peaceful-tundra-33461.herokuapp.com/api/users', {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName : firstName,
						lastName : lastName,
						email : userEmail,
						password : password1,
						mobileNo : mobileNumber
					})
				})
				.then(res => res.json())
				.then(data => {

					console.log(data)

					if(data === true){
						alert("Registered Successfully");
						window.location.replace("login.html");
					} else {
						alert("OOPS! Something went wrong.");
					}
				})

			} else {
				alert("Email has been used. Try a different one")
			}

		})

	} /*else {
		alert("something went wrong")
	}*/
})

