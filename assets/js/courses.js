let token = localStorage.getItem("token")
console.log(token);

// retrieve the user information for isAdmin
let adminUser = localStorage.getItem("isAdmin");

console.log(adminUser);

//will contain the HTML for the different buttons per user
let cardFooter;

fetch('https://peaceful-tundra-33461.herokuapp.com/api/courses')
.then(res => res.json())
.then(data => {

	console.log(data);

	//variable to store the card 
	let courseData;
	
	// message to show if there's no courses
	if(data.length < 1) {
		courseData = "No courses available"
	} else {

		courseData = data.map(course => {

			console.log(course._id);

			//logic for rendering different buttons based on the user
			//User is NOT Admin
			if(adminUser === "false" || !adminUser && course.isActive === true) {
				// how to pass a data via the URL
				// relative_path?parameter_name=value&param_name2=value2
				// course.html?courseId=${course._id}
				cardFooter = 
				//Select course button
				`
				<a href="./course.html?courseId=${course._id}" value=${course._id} class="btn btn-primary text-white btn-block editButton"> Select Course </a>
				`
			//User is Admin
			} else {
				cardFooter = 
					`
					<a href="./course.html?courseId=${course._id}" value=${course._id} class="btn btn-primary text-white btn-block editButton"> View </a>
					<a href="./editCourse.html?courseId=${course._id}" value=${course._id} class="btn btn-primary text-white btn-block editButton"> Edit </a>
					`
						if(course.isActive === true) {
								cardFooter +=
								`
								<a href="./deleteCourse.html?courseId=${course._id}" value=${course._id} class="btn btn-danger text-white btn-block dangerButton">Disable Course</a>
								`
						} 
						else {
							cardFooter +=
							`
							<a href="enableCourse.html?courseId=${course._id}"  value=${course._id} class="btn btn-success btn-block text-white successButton">Enable</a>
							`
						}
			}

			if(!token || token === null){
				cardFooter =
				`
					<a href="./login.html" class="btn btn-primary text-white btn-block editButton"> Select Course </a>
				`
			}

			if((adminUser == 'true') || ((adminUser == 'false' || !adminUser) && course.isActive === true || token !== null) )

			//code reflected on the page
			return (
				`
				<div class="col-md-6 my-3">
				    <div class='card'>
				        <div class='card-body'>
				            <h5 class='card-title'>${course.name}</h5>
				            <p class='card-text text-left'>
				                ${course.description}
				            </p>
				            <p class='card-text text-right'>
				               ₱ ${course.price}
				            </p>

				        </div>
				        <div class='card-footer'>
				            ${cardFooter}
				        </div>
				    </div>
				</div>
				`
			)
		}).join("") // to remove the comma in the Array syntax

		//courseData = [{course1}, {course2}, ....]

		let coursesContainer = document.querySelector("#coursesContainer");

		coursesContainer.innerHTML = courseData;
	}
})

let modalButton = document.querySelector('#adminButton');

if(adminUser == "false" || !adminUser) {
	modalButton.innerHTML = null;
} else {
	modalButton.innerHTML =
	`
	<div class="col-md-2 offset-md-10">
		<a href="./addCourse.html" class="btn btn-block btn-primary"> Add Course </a>
	</div>
	`
}


/*<a href="./deleteCourse.html?courseId=${course._id}" value=${course._id} class="btn btn-danger text-white btn-block dangerButton"> Disable Course </a>*/

